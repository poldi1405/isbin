package main

import (
	"fmt"

	"gitlab.com/poldi1405/go-ansi"
	"gitlab.com/poldi1405/isbin/detector"
)

var (
	binaryFormatter func(...interface{}) string
	textFormatter   func(...interface{}) string
)

func checkFile(path string) {
	defer wg.Done()

	if gitmode {
		fmt.Println("git mode")
		isGit, root := isGit(path)
		if isGit {
			isIgnored, err := isIgnored(path)
			isLfs, err := isLFS(path)
			if isIgnored {
				msgPrinter <- "[" + ansi.Yellow("IGNORE") + "]\t" + path
				return
			}
			if isLfs {
				msgPrinter <- "[" + ansi.Yellow("IN LFS") + "]\t" + path
				return
			}
			if err != nil {
				msgPrinter <- "[" + ansi.Yellow(" WARN ") + "]\t" + err.Error()
			}
		}
	}

	bin, err := detector.IsBin(path)
	if err != nil {
		msgPrinter <- "[" + ansi.Red("ERROR!") + "]\t" + err.Error()
		exitError = 1
		return
	}

	if bin {
		msgPrinter <- "[" + binaryFormatter("BINARY") + "]\t" + path
	} else {
		msgPrinter <- "[" + textFormatter(" TEXT ") + "]\t" + path
	}
	if invert && bin {
		exitError = 1
	}
}
