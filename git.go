package main

import (
	"os"
	"strings"

	_ "github.com/denormal/go-gitignore"
)

func isGit(path string) (result bool, root string) {
	for i := 0; i < 30; i++ {
		up := strings.Repeat(".."+string(os.PathSeparator), i)
		checkpath := path + string(os.PathSeparator) + up + ".git" + os.PathSeparator
		if _, err := os.Stat(checkpath); !os.IsNotExist(err) {
			return true, path.Clean(checkpath)
		}
	}
	return false, nil
}

func isIgnored(path string, root string) bool,error {

}
