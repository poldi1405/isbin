module gitlab.com/poldi1405/isbin

go 1.15

require (
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964 // indirect
	github.com/denormal/go-gitignore v0.0.0-20180930084346-ae8ad1d07817
	github.com/leaanthony/clir v1.0.4
	gitlab.com/poldi1405/go-ansi v1.1.0
)
